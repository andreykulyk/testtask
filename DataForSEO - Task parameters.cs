﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestTask
{
    public partial class MainForm : Form
    {
        private const string LOCATION_LIST_URL = "/v3/serp/bing/locations/es";
        private const string LANGUAGE_LIST_URL = "/v3/serp/bing/languages";

        private const string EMPTY_VALUE = "-";
        private const int NO_SEARCH_RESULTS_CODE = 40102;
        private const int TASK_CREATED_CODE = 20100;

        private int currentRaw = 0;

        private bool isLocation = true;
        public MainForm()
        {
            InitializeComponent();
            ConfigurationPostdata();
        }

        public string ReturnSearchEngine()
        {
            string se = "";
            if (this.SE_Google.Checked)
            {
                se = "google";
            }
            else if (this.SE_Bing.Checked)
            {
                se = "bing";
            }
            else
            {
                se = "";
            }
            return se;
        }

        private void Results_table_Paint(object sender, PaintEventArgs e)
        {

        }

        private async void SendButton_Click(object sender, EventArgs e)
        {
            PostData postData = new PostData(this);
            TaskDataModel task = new TaskDataModel();
            LiveMethod(await SerpLive.serp_live_advanced(postData), postData, task, currentRaw);
            
            
           // SqliteDataAccess.SaveTask(task);
        }
        private async void SendButtonSecond_Click(object sender, EventArgs e)
        {
            PostData postData = new PostData(this);
            TaskDataModel task = new TaskDataModel();
            DelayedMethod(await TaskPost.serp_task_post(postData), postData, task, currentRaw);
           // SqliteDataAccess.SaveTask(task);

        }

        private async void ConfigurationPostdata()
        {
            string locationResult = await GetJson(LOCATION_LIST_URL);
            ParseJson(locationResult, isLocation);
            string languageResult = await GetJson(LANGUAGE_LIST_URL);
            ParseJson(languageResult, !isLocation);
        }

        private void ParseJson(string contentResult, bool isLocation)
        {
            try
            {
                var task = JsonConvert.DeserializeObject<dynamic>(contentResult);

                for (int i = 0; i < task.tasks.Count; i++)
                {

                    for (int j = 0; j < task.tasks[i].result.Count; j++)
                    {
                        if (isLocation)
                        {
                            this.Location_ComboBox.Items.Add(task.tasks[i].result[j].location_name);
                        }
                        else if (!isLocation)
                        {
                            this.Language_ComboBox.Items.Add(task.tasks[i].result[j].language_name);
                        }

                    }
                }
                SetDefaultIndex(isLocation);
            }
            catch
            {
                Console.WriteLine("Could not load the json for settings");
            }

        }

        private void LiveMethod(string contentResult, PostData postData, TaskDataModel taskData, int currentRaw)
        {
            try {
                currentRaw = Result_Table.Rows.Add();

                var task = JsonConvert.DeserializeObject<dynamic>(contentResult);
                ParseResponseJSON(task, postData, taskData, currentRaw);
            }
            catch
            {
                SetEmptyValues(taskData, currentRaw);
                ShowIncorrectParametersDialogue();
                SqliteDataAccess.SaveTask(taskData);
                return;
            }
        }
        private async void DelayedMethod(string contentResult, PostData postData, TaskDataModel taskData, int currentRaw)
        {
            currentRaw = Result_Table.Rows.Add();
            string id = "";
            var task = JsonConvert.DeserializeObject<dynamic>(contentResult);
           

            for (int i = 0; i < task.tasks.Count; i++)
            {
                id = task.tasks[i].id;
                Result_Table.Rows[currentRaw].Cells["TaskID"].Value = id;
                taskData.TaskID = id;

                Result_Table.Rows[currentRaw].Cells["Status"].Value = task.tasks[i].status_code;
                taskData.StatusCode = task.tasks[i].status_code;

                if (task.tasks[i].status_code != TASK_CREATED_CODE)
                {
                    SetEmptyValues(taskData, currentRaw);
                    ShowIncorrectParametersDialogue();         
                    SqliteDataAccess.SaveTask(taskData);
                    return;
                }
            }

            bool callTaskReady = await TaskReady.serp_tasks_ready(postData.searchEngine, id);
            while (!callTaskReady)
            {
                await Task.Delay(15000);
                callTaskReady = await TaskReady.serp_tasks_ready(postData.searchEngine, id);
            }


            var result = JsonConvert.DeserializeObject<dynamic>(await TaskGet.serp_task_get_by_id(id));

            ParseResponseJSON(result, postData, taskData, currentRaw);

           
        }
        private void ParseResponseJSON(dynamic task, PostData postData, TaskDataModel taskData, int currentRaw)
        {
            Result_Table.Rows[currentRaw].Cells["Website"].Value = postData.website;
            taskData.Website = postData.website;

            for (int i = 0; i < task.tasks.Count; i++)
            {

                Result_Table.Rows[currentRaw].Cells["FunctionName"].Value = task.tasks[i].data.function;

                Result_Table.Rows[currentRaw].Cells["Status"].Value = task.tasks[i].status_code;
                taskData.StatusCode = task.tasks[i].status_code;

                Result_Table.Rows[currentRaw].Cells["TaskID"].Value = task.tasks[i].id;
                taskData.TaskID = task.tasks[i].id;

                Result_Table.Rows[currentRaw].Cells["SearchEngine"].Value = task.tasks[i].data.se;
                taskData.SearchEngine = task.tasks[i].data.se;

                for (int j = 0; j < task.tasks[i].result.Count; j++)
                {
                    Result_Table.Rows[currentRaw].Cells["Keyword"].Value = task.tasks[i].result[j].keyword;
                    taskData.Keyword = task.tasks[i].result[j].keyword;

                    if (task.tasks[i].status_code == NO_SEARCH_RESULTS_CODE)
                    {

                        Result_Table.Rows[currentRaw].Cells["Position"].Value = EMPTY_VALUE;
                        taskData.Position = EMPTY_VALUE;
                        break;

                    }

                    for (int k = 0; k < task.tasks[i].result[j].items.Count; k++)
                    {

                        if (task.tasks[i].result[j].items[k].domain == postData.website)
                        {
                            Result_Table.Rows[currentRaw].Cells["Position"].Value = task.tasks[i].result[j].items[k].rank_absolute;
                            taskData.Position = task.tasks[i].result[j].items[k].rank_absolute;
                            break;
                        }

                        else
                        {
                            Result_Table.Rows[currentRaw].Cells["Position"].Value = EMPTY_VALUE;
                            taskData.Position = EMPTY_VALUE;
                        }

                    }
                }
            }
            SqliteDataAccess.SaveTask(taskData);

        }

        


        private async Task<String> GetJson(string url)
        { 
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://api.dataforseo.com/"),
                DefaultRequestHeaders = { Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes("support@dataforseo.com:IuJlEv1M3fVh9HF3"))) }
            };

            var response = await httpClient.GetAsync(url);
            var contentResult = await response.Content.ReadAsStringAsync();
            return contentResult;
        }

        private void SetDefaultIndex(bool isLocation)
        {
            if (isLocation)
            {
                this.Location_ComboBox.SelectedIndex = 1;
            }
            else
            {
                this.Language_ComboBox.SelectedIndex = 1;
            }
                       
        }
        private void SetEmptyValues(TaskDataModel taskData, int currentRaw)
        {
            taskData.SearchEngine = EMPTY_VALUE;
            taskData.Keyword = EMPTY_VALUE;
            taskData.Website = EMPTY_VALUE;
            taskData.Position = EMPTY_VALUE;



            Result_Table.Rows[currentRaw].Cells["SearchEngine"].Value = EMPTY_VALUE;
            Result_Table.Rows[currentRaw].Cells["Keyword"].Value = EMPTY_VALUE;
            Result_Table.Rows[currentRaw].Cells["Website"].Value = EMPTY_VALUE;
            Result_Table.Rows[currentRaw].Cells["Position"].Value = EMPTY_VALUE;

        }
        
        
        private void ShowIncorrectParametersDialogue()
        {
            this.Refresh();
            MessageBox.Show(
                            "Введите все параметры для отправки запроса",
                            "Введены не все параметры",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information,
                            MessageBoxDefaultButton.Button1,
                            MessageBoxOptions.DefaultDesktopOnly);

        }
    }
}
    
