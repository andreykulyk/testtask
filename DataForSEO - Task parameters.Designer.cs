﻿
namespace TestTask
{
   public partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Location_Label = new System.Windows.Forms.Label();
            this.Location_ComboBox = new System.Windows.Forms.ComboBox();
            this.SE_GroupBox = new System.Windows.Forms.GroupBox();
            this.SE_Bing = new System.Windows.Forms.RadioButton();
            this.SE_Google = new System.Windows.Forms.RadioButton();
            this.Keyword_TextBox = new System.Windows.Forms.TextBox();
            this.Keyword_Label = new System.Windows.Forms.Label();
            this.Result_Table = new System.Windows.Forms.DataGridView();
            this.TaskID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Keyword = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SearchEngine = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Website = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Position = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FunctionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SendButton = new System.Windows.Forms.Button();
            this.Language_ComboBox = new System.Windows.Forms.ComboBox();
            this.Language_Label = new System.Windows.Forms.Label();
            this.Website_label = new System.Windows.Forms.Label();
            this.Website_TextBox = new System.Windows.Forms.TextBox();
            this.SendButtonSecond = new System.Windows.Forms.Button();
            this.SE_GroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Result_Table)).BeginInit();
            this.SuspendLayout();
            // 
            // Location_Label
            // 
            this.Location_Label.AutoSize = true;
            this.Location_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Location_Label.Location = new System.Drawing.Point(125, 107);
            this.Location_Label.Name = "Location_Label";
            this.Location_Label.Size = new System.Drawing.Size(76, 21);
            this.Location_Label.TabIndex = 2;
            this.Location_Label.Text = "Location";
            // 
            // Location_ComboBox
            // 
            this.Location_ComboBox.DisplayMember = "0";
            this.Location_ComboBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Location_ComboBox.Location = new System.Drawing.Point(12, 131);
            this.Location_ComboBox.Name = "Location_ComboBox";
            this.Location_ComboBox.Size = new System.Drawing.Size(312, 29);
            this.Location_ComboBox.TabIndex = 3;
            this.Location_ComboBox.ValueMember = "0";
            // 
            // SE_GroupBox
            // 
            this.SE_GroupBox.Controls.Add(this.SE_Bing);
            this.SE_GroupBox.Controls.Add(this.SE_Google);
            this.SE_GroupBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SE_GroupBox.Location = new System.Drawing.Point(12, 12);
            this.SE_GroupBox.Name = "SE_GroupBox";
            this.SE_GroupBox.Size = new System.Drawing.Size(200, 83);
            this.SE_GroupBox.TabIndex = 4;
            this.SE_GroupBox.TabStop = false;
            this.SE_GroupBox.Text = "SE";
            // 
            // SE_Bing
            // 
            this.SE_Bing.AutoSize = true;
            this.SE_Bing.Location = new System.Drawing.Point(6, 42);
            this.SE_Bing.Name = "SE_Bing";
            this.SE_Bing.Size = new System.Drawing.Size(62, 25);
            this.SE_Bing.TabIndex = 1;
            this.SE_Bing.TabStop = true;
            this.SE_Bing.Text = "Bing";
            this.SE_Bing.UseVisualStyleBackColor = true;
            // 
            // SE_Google
            // 
            this.SE_Google.AutoSize = true;
            this.SE_Google.Checked = true;
            this.SE_Google.Location = new System.Drawing.Point(6, 19);
            this.SE_Google.Name = "SE_Google";
            this.SE_Google.Size = new System.Drawing.Size(83, 25);
            this.SE_Google.TabIndex = 0;
            this.SE_Google.TabStop = true;
            this.SE_Google.Text = "Google";
            this.SE_Google.UseVisualStyleBackColor = true;
            // 
            // Keyword_TextBox
            // 
            this.Keyword_TextBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Keyword_TextBox.Location = new System.Drawing.Point(71, 324);
            this.Keyword_TextBox.Name = "Keyword_TextBox";
            this.Keyword_TextBox.Size = new System.Drawing.Size(200, 29);
            this.Keyword_TextBox.TabIndex = 5;
            // 
            // Keyword_Label
            // 
            this.Keyword_Label.AutoSize = true;
            this.Keyword_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Keyword_Label.Location = new System.Drawing.Point(125, 300);
            this.Keyword_Label.Name = "Keyword_Label";
            this.Keyword_Label.Size = new System.Drawing.Size(79, 21);
            this.Keyword_Label.TabIndex = 6;
            this.Keyword_Label.Text = "Keyword";
            // 
            // Result_Table
            // 
            this.Result_Table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Result_Table.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TaskID,
            this.Keyword,
            this.SearchEngine,
            this.Status,
            this.Website,
            this.Position,
            this.FunctionName});
            this.Result_Table.Location = new System.Drawing.Point(396, 41);
            this.Result_Table.Name = "Result_Table";
            this.Result_Table.Size = new System.Drawing.Size(834, 163);
            this.Result_Table.TabIndex = 7;
            // 
            // TaskID
            // 
            this.TaskID.HeaderText = "TaskID";
            this.TaskID.Name = "TaskID";
            this.TaskID.Width = 200;
            // 
            // Keyword
            // 
            this.Keyword.HeaderText = "Keyword";
            this.Keyword.Name = "Keyword";
            // 
            // SearchEngine
            // 
            this.SearchEngine.HeaderText = "SearchEngine";
            this.SearchEngine.Name = "SearchEngine";
            // 
            // Status
            // 
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            // 
            // Website
            // 
            this.Website.HeaderText = "Website";
            this.Website.Name = "Website";
            // 
            // Position
            // 
            this.Position.HeaderText = "Position";
            this.Position.Name = "Position";
            // 
            // FunctionName
            // 
            this.FunctionName.HeaderText = "FunctionName";
            this.FunctionName.Name = "FunctionName";
            // 
            // SendButton
            // 
            this.SendButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.SendButton.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.SendButton.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SendButton.Location = new System.Drawing.Point(71, 394);
            this.SendButton.Name = "SendButton";
            this.SendButton.Size = new System.Drawing.Size(185, 44);
            this.SendButton.TabIndex = 8;
            this.SendButton.Text = "Send (Live)";
            this.SendButton.UseVisualStyleBackColor = false;
            this.SendButton.UseWaitCursor = true;
            this.SendButton.Click += new System.EventHandler(this.SendButton_Click);
            // 
            // Language_ComboBox
            // 
            this.Language_ComboBox.DisplayMember = "0";
            this.Language_ComboBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Language_ComboBox.FormattingEnabled = true;
            this.Language_ComboBox.Location = new System.Drawing.Point(33, 196);
            this.Language_ComboBox.Name = "Language_ComboBox";
            this.Language_ComboBox.Size = new System.Drawing.Size(264, 29);
            this.Language_ComboBox.TabIndex = 9;
            this.Language_ComboBox.ValueMember = "0";
            // 
            // Language_Label
            // 
            this.Language_Label.AutoSize = true;
            this.Language_Label.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Language_Label.Location = new System.Drawing.Point(123, 172);
            this.Language_Label.Name = "Language_Label";
            this.Language_Label.Size = new System.Drawing.Size(81, 21);
            this.Language_Label.TabIndex = 10;
            this.Language_Label.Text = "Language";
            // 
            // Website_label
            // 
            this.Website_label.AutoSize = true;
            this.Website_label.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Website_label.Location = new System.Drawing.Point(132, 233);
            this.Website_label.Name = "Website_label";
            this.Website_label.Size = new System.Drawing.Size(69, 21);
            this.Website_label.TabIndex = 11;
            this.Website_label.Text = "Website";
            // 
            // Website_TextBox
            // 
            this.Website_TextBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Website_TextBox.Location = new System.Drawing.Point(71, 257);
            this.Website_TextBox.Name = "Website_TextBox";
            this.Website_TextBox.Size = new System.Drawing.Size(200, 29);
            this.Website_TextBox.TabIndex = 12;
            // 
            // SendButtonSecond
            // 
            this.SendButtonSecond.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.SendButtonSecond.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.SendButtonSecond.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SendButtonSecond.Location = new System.Drawing.Point(987, 394);
            this.SendButtonSecond.Name = "SendButtonSecond";
            this.SendButtonSecond.Size = new System.Drawing.Size(185, 44);
            this.SendButtonSecond.TabIndex = 13;
            this.SendButtonSecond.Text = "Send (Delayed)";
            this.SendButtonSecond.UseVisualStyleBackColor = false;
            this.SendButtonSecond.UseWaitCursor = true;
            this.SendButtonSecond.Click += new System.EventHandler(this.SendButtonSecond_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(1235, 482);
            this.Controls.Add(this.SendButtonSecond);
            this.Controls.Add(this.Website_TextBox);
            this.Controls.Add(this.Website_label);
            this.Controls.Add(this.Language_Label);
            this.Controls.Add(this.Language_ComboBox);
            this.Controls.Add(this.SendButton);
            this.Controls.Add(this.Result_Table);
            this.Controls.Add(this.Keyword_Label);
            this.Controls.Add(this.Keyword_TextBox);
            this.Controls.Add(this.SE_GroupBox);
            this.Controls.Add(this.Location_ComboBox);
            this.Controls.Add(this.Location_Label);
            this.Name = "MainForm";
            this.Text = "DataForSEO Task Settings";
            this.SE_GroupBox.ResumeLayout(false);
            this.SE_GroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Result_Table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Location_Label;
        public System.Windows.Forms.ComboBox Location_ComboBox;
        private System.Windows.Forms.GroupBox SE_GroupBox;
        public System.Windows.Forms.RadioButton SE_Bing;
        public System.Windows.Forms.RadioButton SE_Google;
        public System.Windows.Forms.TextBox Keyword_TextBox;
        private System.Windows.Forms.Label Keyword_Label;
        public System.Windows.Forms.DataGridView Result_Table;
        private System.Windows.Forms.Button SendButton;
        public System.Windows.Forms.ComboBox Language_ComboBox;
        private System.Windows.Forms.Label Language_Label;
        private System.Windows.Forms.Label Website_label;
        public System.Windows.Forms.TextBox Website_TextBox;
        private System.Windows.Forms.DataGridViewTextBoxColumn TaskID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Keyword;
        private System.Windows.Forms.DataGridViewTextBoxColumn SearchEngine;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Website;
        private System.Windows.Forms.DataGridViewTextBoxColumn Position;
        private System.Windows.Forms.Button SendButtonSecond;
        private System.Windows.Forms.DataGridViewTextBoxColumn FunctionName;
    }
}

