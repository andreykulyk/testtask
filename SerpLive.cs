﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    public static class SerpLive
    {
        public static async Task<string> serp_live_advanced(PostData postArray)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://api.dataforseo.com/"),
                DefaultRequestHeaders = { Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes("support@dataforseo.com:IuJlEv1M3fVh9HF3"))) }
            };
            var postData = new List<object>();

            postData.Add(new
            {
                language_name = postArray.language,
                location_name = postArray.location,
                keyword = postArray.keyword
            });
            var taskPostResponse = await httpClient.PostAsync($"/v3/serp/{postArray.searchEngine}/organic/live/regular", new StringContent(JsonConvert.SerializeObject(postData)));
            var result = JsonConvert.DeserializeObject<dynamic>(await taskPostResponse.Content.ReadAsStringAsync());

            if (result.status_code == 20000)
            {
                Console.WriteLine(result);
            }
            else
            {
                Console.WriteLine($"error. Code: {result.status_code} Message: {result.status_message}");
            }
            return await taskPostResponse.Content.ReadAsStringAsync();
        }
    }
}