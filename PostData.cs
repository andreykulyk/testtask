﻿using System;


namespace TestTask
{
    public class  PostData
    {
        MainForm mainForm;

        public string keyword;
        public string searchEngine;
        public string location;
        public string language;
        public string website;

        public PostData(MainForm mainForm)
        {
            this.mainForm = mainForm;
            Initialize();
        }
        private void Initialize()
        {
            keyword = mainForm.Keyword_TextBox.Text;
            website = mainForm.Website_TextBox.Text;
            searchEngine = mainForm.ReturnSearchEngine();  
            location = mainForm.Location_ComboBox.SelectedItem?.ToString();
            language = mainForm.Language_ComboBox.SelectedItem?.ToString();
            Console.WriteLine("search engine:" + searchEngine + "keyword:" + keyword + "location:" + location + "language" + language + "website" + website);
        }
    }
}

