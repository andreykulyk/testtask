﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;
using Dapper;

namespace TestTask
{
   public class SqliteDataAccess
    {
        public static List<TaskDataModel> LoadTasks()
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<TaskDataModel>("select * from Tasks", new DynamicParameters());
                return output.ToList();
            }

        }

        public static void SaveTask(TaskDataModel task)
        {
            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                cnn.Execute("insert into Tasks (TaskID, Keyword, Website, SearchEngine, Position, StatusCode)" +
                    " values (@TaskID, @Keyword, @Website, @SearchEngine, @Position, @StatusCode)", task);
            }

        }
      
        public static List<TaskDataModel> GetTask(string position)
        {

            using (IDbConnection cnn = new SQLiteConnection(LoadConnectionString()))
            {
                var output = cnn.Query<TaskDataModel>(@"select * from Tasks WHERE Position = @Position", new { Position = position });
                return output.ToList();

            }
        }

        private static string LoadConnectionString(string id = "Default")
        {
            return ConfigurationManager.ConnectionStrings[id].ConnectionString;
        }
        

    }

}
