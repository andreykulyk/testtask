﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    public static partial class TaskReady
    {
        public static async Task<bool> serp_tasks_ready(string searchEngine, string id)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://api.dataforseo.com/"),

                DefaultRequestHeaders = { Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes("support@dataforseo.com:IuJlEv1M3fVh9HF3"))) }
            };

            var response = await httpClient.GetAsync($"/v3/serp/{searchEngine}/organic/tasks_ready");
            var task = JsonConvert.DeserializeObject<dynamic>(await response.Content.ReadAsStringAsync());

            if (task.status_code == 20000)
            {

                Console.WriteLine(task);
            }
            else
                Console.WriteLine($"error. Code: {task.status_code} Message: {task.status_message}");

            bool flag = false;

            int result_count = (int)task.tasks[0].result_count;
            Console.WriteLine(result_count);


            if (result_count > 0)
            {
                for (int i = 0; i < task.tasks.Count; i++)
                {
                    Console.WriteLine(id);
                    for (int j = 0; j < task.tasks[i].result.Count; j++)
                    {
                        if (id == (string)task.tasks[i].result[j].id)
                        {
                            flag = true;
                            break;
                        }
                    }
                }
            }
            return flag;
        }
    } 
}


