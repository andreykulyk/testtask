﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    public static partial class TaskGet
    {
        public static async Task<string> serp_task_get_by_id(string id)
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://api.dataforseo.com/"),

                DefaultRequestHeaders = { Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes("support@dataforseo.com:IuJlEv1M3fVh9HF3"))) }
            };
          
          
            var taskGetResponse = await httpClient.GetAsync("/v3/serp/google/organic/task_get/regular/" + id);
            var result = JsonConvert.DeserializeObject<dynamic>(await taskGetResponse.Content.ReadAsStringAsync());

            if (result.tasks != null)
            {
                var fst = result.tasks.First;
                if (fst.status_code >= 40000 || fst.result == null)
                    Console.WriteLine($"error. Code: {fst.status_code} Message: {fst.status_message}");
                else
                    Console.WriteLine(String.Join(Environment.NewLine, fst));
            }
            else
                Console.WriteLine($"error. Code: {result.status_code} Message: {result.status_message}");

            return await taskGetResponse.Content.ReadAsStringAsync();
        }
    }
}
