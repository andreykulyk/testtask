﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    public class TaskDataModel
    {
        public string TaskID { get; set; }
        public string Website { get; set; }
        public string Keyword { get; set; }
        public string SearchEngine { get; set; }
        public string Position { get; set; }
        public int  StatusCode { get; set; }


    }
}
